<?php

namespace App\Console\Commands;

use App\Lib\RabbitMQConsumerBase;
use Illuminate\Console\Command;

class MessageConsumer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'consumer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'message consumer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //

        $config = [];
        $config["host"] = "127.0.0.1";
        $config["port"] = "5672";
        $config["user"] = "admin";
        $config["password"] = "admin";
        $config["exchange"] = ["change" => "fanout","change1" => "topic"];
        $config["queue"] = "test";
        $config["vhost"] = "/";
        $config["bindingKeys"] = [
            "change" => ["test","test1"],
            "change" => ["test","test1"],
        ];
        $config["routingKey"] = "test";

        $rabbit = new RabbitMQConsumerBase($config);
        $rabbit->run([$this,"callback"]);
    }

    public function callback($msg){
        echo $msg."\n";
        return true;
    }
}
