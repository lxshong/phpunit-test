<?php

namespace App\Console\Commands;

use App\Lib\RabbitMQProducerBase;
use Illuminate\Console\Command;

class MessageProducer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'producer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'produce message';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //
        $config = [];
        $config["host"] = "127.0.0.1";
        $config["port"] = "5672";
        $config["user"] = "admin";
        $config["password"] = "admin";
        $config["exchange"] = "test";
        $config["queue"] = "test";
        $config["vhost"] = "/";
        $config["routingKey"] = "test21";
        $config["exchangeType"] = "fanout";
        $rabbit = new RabbitMQProducerBase($config);
        $rabbit->send("this is a test21",$config["routingKey"]);
        echo "send success\n";
    }
}
