<?php
/**
 * Created by PhpStorm.
 * User: liuxiaosong
 * Date: 2019/3/20
 * Time: 上午12:20
 */

namespace App\Lib;


use PhpAmqpLib\Connection\AMQPStreamConnection;

/**
 * $config = [];
 * $config["host"] = "127.0.0.1";
 * $config["port"] = "5672";
 * $config["user"] = "admin";
 * $config["password"] = "admin";
 * $config["exchange"] = [
 *      "change" => "fanout",
 *      "change1" => "topic",
 * ];
 * $config["queue"] = "test";
 * $config["vhost"] = "/";
 * $config["bindingKeys"] = [
 *      "change" => ["test","test1"],
 *      "change1" => ["test","test1"],
 * ];
 */
class RabbitMQConsumerBase
{
    protected $_host = null;
    protected $_port = null;
    protected $_username = null;
    protected $_password = null;
    protected $_connection = null;
    protected $_vhost = null;
    protected $_exchange = [];
    protected $_queue = null;
    protected $_bindingKeys = [];

    protected $_channel = null;

    /**
     * RabbitMqConsumerBase constructor.
     */
    public function __construct($config)
    {
        $this->_host = isset($config["host"]) ? $config["host"] : null;
        $this->_port = isset($config["port"]) ? $config["port"] : null;
        $this->_username = isset($config["user"]) ? $config["user"] : null;
        $this->_password = isset($config["password"]) ? $config["password"] : null;
        $this->_vhost = isset($config["vhost"]) ? $config["vhost"] : null;
        $this->_exchange = isset($config["exchange"]) ? $config["exchange"] : [];
        $this->_queue = isset($config["queue"]) ? $config["queue"] : null;
        $this->_bindingKeys = isset($config["bindingKeys"]) ? $config["bindingKeys"] : ["#"];
        $this->connect();
    }

    protected function connect()
    {
        $this->_connection = new AMQPStreamConnection(
            $this->_host,
            $this->_port,
            $this->_username,
            $this->_password,
            $this->_vhost
        );

        $this->_channel = $this->_connection->channel();
        $this->declareExchange($this->_exchange);
        list($queue_name, ,) = $this->_channel->queue_declare($this->_queue, false, true, false, false);
        foreach ($this->_bindingKeys as $exchange => $keys) {
            $this->bindQueue($queue_name, $exchange, $keys);
        }
    }

    protected function declareExchange($exchange)
    {
        foreach ($exchange as $e => $type) {
            $this->_channel->exchange_declare($e, $type, false, true, false);
        }
    }

    protected function bindQueue($queueName, $exchange, $bindingKeys = "")
    {
        if (is_array($bindingKeys)) {
            foreach ($bindingKeys as $bindingKey) {
                $this->_channel->queue_bind($queueName, $exchange, $bindingKey);
            }
        } else {
            $this->_channel->queue_bind($queueName, $exchange, $bindingKeys);
        }
    }

    public function run($callback)
    {
        $this->_channel->basic_consume($this->_queue, '', false, false, false, false, function ($msg) use ($callback) {
            if (call_user_func($callback, $msg->body)) {
                $msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
            }
        });

        while (count($this->_channel->callbacks)) {
            $this->_channel->wait();
        }
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        if (!is_null($this->_connection)) {
            $this->_connection->close();
        }

        if (!is_null($this->_channel)) {
            $this->_channel->close();
        }
    }
}