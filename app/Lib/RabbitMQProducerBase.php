<?php
/**
 * Created by PhpStorm.
 * User: liuxiaosong
 * Date: 2019/3/20
 * Time: 上午12:06
 */

namespace App\Lib;


use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

class RabbitMQProducerBase
{
    protected $_host = null;
    protected $_port = null;
    protected $_username = null;
    protected $_password = null;
    protected $_connection = null;
    protected $_vhost = null;
    protected $_exchange = null;
    protected $_queue = null;
    protected $_waitTimeout = 2;

    protected $_channel = null;
    public $_exchangeType = "topic";

    /**
     * RabbitMqConsumerBase constructor.
     */
    public function __construct($config)
    {
        $this->_host = isset($config["host"]) ? $config["host"] : null;
        $this->_port = isset($config["port"]) ? $config["port"] : null;
        $this->_username = isset($config["user"]) ? $config["user"] : null;
        $this->_password = isset($config["password"]) ? $config["password"] : null;
        $this->_vhost = isset($config["vhost"]) ? $config["vhost"] : null;
        $this->_exchange = isset($config["exchange"]) ? $config["exchange"] : null;
        $this->_exchangeType = isset($config["exchangeType"]) ? $config["exchangeType"] : "topic";
        $this->connect();
    }

    protected function connect()
    {
        $this->_connection = new AMQPStreamConnection(
            $this->_host,
            $this->_port,
            $this->_username,
            $this->_password,
            $this->_vhost
        );

        $this->_channel = $this->_connection->channel();
        $this->_channel->exchange_declare($this->_exchange, $this->_exchangeType, false, true, false);
        $this->_channel->confirm_select();
        $this->_channel->wait_for_pending_acks();
    }

    public function send($msg, $routingKey)
    {
        if (!is_string($msg)) {
            $msg = json_encode($msg);
        }
        $msg = new AMQPMessage($msg, ['delivery_mode' => 2]);

        $this->_channel->basic_publish($msg, $this->_exchange, $routingKey);
        $this->_channel->wait_for_pending_acks($this->_waitTimeout);
        return true;

    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        if (!is_null($this->_connection)) {
            $this->_connection->close();
        }

        if (!is_null($this->_channel)) {
            $this->_channel->close();
        }
    }
}