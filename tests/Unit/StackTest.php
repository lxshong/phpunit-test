<?php
/**
 * Created by PhpStorm.
 * User: liuxiaosong
 * Date: 2019/3/14
 * Time: 下午4:56
 */

namespace Tests\Unit;


use Tests\TestCase;

class StackTest extends TestCase
{
    protected $stack;
    protected function setUp()
    {
        $this->stack = [];
    }
    public function testEmpty()
    {
        $this->assertTrue(empty($this->stack));
    }
    public function testPush()
    {
        array_push($this->stack, 'foo');
        $this->assertEquals('foo', $this->stack[count($this->stack)-1]);
        $this->assertFalse(empty($this->stack));
    }

}