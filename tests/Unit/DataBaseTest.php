<?php
/**
 * Created by PhpStorm.
 * User: liuxiaosong
 * Date: 2019/3/14
 * Time: 下午5:18
 */

namespace Tests\Unit;


use PHPUnit\ExampleExtension\TestCaseTrait;
use Tests\TestCase;

class DataBaseTest extends TestCase
{
    use TestCaseTrait;
    static private $pdo = null;
    private $conn = null;
    final public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new PDO($GLOBALS["DB_DSN"],$GLOBALS["DB_USER"],$GLOBALS["DB_PASSWD"]);
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo, $GLOBALS["DB_DBNAME"]);
        }
        return $this->conn;
    }

}