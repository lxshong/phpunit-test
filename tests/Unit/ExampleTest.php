<?php

namespace Tests\Unit;

use App\Models\Student;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ExampleTest extends TestCase
{

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicTest()
    {
        $this->assertTrue(true);
    }

    /**
     * testEmptyStack
     * @return array
     */
    public function testEmptyStack()
    {
        $stack = [];
        $this->assertEmpty($stack);
        return $stack;
    }

    /**
     * testEmptyStack1
     * @return array
     */
    public function testEmptyStack1()
    {
        $stack = [];
        $this->assertEmpty($stack);
        return $stack;
    }

    /**
     * 简单依赖测试
     * @depends testEmptyStack
     */
    public function testPushStack($stack)
    {
        array_push($stack, "test");
        $this->assertEquals(count($stack), 1);
    }


    /**
     * @depends testEmptyStack
     * @depends testEmptyStack1
     * @param $stack
     */
    public function testMultiDepends($stack, $stack1)
    {
        array_push($stack, "test");
        $this->assertEquals(count($stack), 1);
        $this->assertEquals(count($stack1), 0);
    }


    /**
     * 测试数据供给器
     * @dataProvider dataProvider
     * @param $first
     * @param $second
     * @param $expected
     */
    public function testDataProvider($first, $second, $expected)
    {
        $this->assertEquals($expected, $first + $second);
    }

    public function dataProvider()
    {
        return [
            "d1" => [0, 0, 0],
            "d2" => [1, 0, 1],
            "d3" => [1, 1, 2],
            "d4" => [0, 1, 1],
        ];
    }

    /**
     * 测试依赖
     */
    public function testExpectprovider()
    {
        $this->assertTrue(true);
        return 1;
    }

    /**
     * @depends      testExpectprovider
     * @dataProvider dataProvider
     * testMultiTypeDepends
     * @param $f
     * @param $s
     * @param $e
     */
    public function testMultiTypeDepends($f, $s, $e)
    {
        $this->assertEquals($e, $f + $s);
    }

    public function dataMultiTypeProvider()
    {
        return [
            "d1" => [0, 1],
            "d2" => [1, 0],
        ];
    }

    /**
     * 对异常进行测试
     * @throws \Exception
     * @expectedException Exception
     * @expectExceptionMessage exception test
     */
    public function testThrowException()
    {
        $student = new Student();
        $student->run();

    }

    /**
     * testError
     * @expectedException ErrorException
     * @expectedException include(123.php): failed to open stream: No such file or directory
     */
    public function testError()
    {
        include "123.php";
    }

    /**
     * testOutPut
     */
    public function testOutPut()
    {
        $this->expectOutputString("lxs");
        print_r((new Student())->getName());
    }
}
